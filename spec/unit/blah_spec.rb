require "blah"

describe Blah do
  describe "::hello" do
    it 'returns "hello"' do
      expect(Blah.hello).to eq("hello")
    end
  end
end